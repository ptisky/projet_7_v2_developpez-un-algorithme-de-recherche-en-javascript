/**
 * Get filter
 * @param {Array} content
 * @param {string} type
 * @param {string} valueFiltered
 * @returns {Array} ValueReturned
 */
function getFilter(content, type, valueFiltered = "nothing") {
    const ValueReturned = [];

    if (type === "ingredients") {
        content.forEach((list) => {
            if (list.ingredients) {
                list.ingredients.forEach((list) => {
                    if (ValueReturned.includes(list.ingredient) === false) {
                        if (valueFiltered == "nothing") {
                            ValueReturned.push(list.ingredient);
                        } else {
                            let ingredientsOk = list.ingredient
                                .toLowerCase()
                                .includes(valueFiltered);

                            if (ingredientsOk === true) {
                                ValueReturned.push(list.ingredient);
                            }
                        }
                    }
                });
            }
        });
    }

    if (type === "appliances") {
        content.forEach((list) => {
            if (ValueReturned.includes(list.appliance) === false) {
                if (valueFiltered == "nothing") {
                    ValueReturned.push(list.appliance);
                } else {
                    let applianceOk = list.appliance
                        .toLowerCase()
                        .includes(valueFiltered);

                    if (applianceOk === true) {
                        ValueReturned.push(list.appliance);
                    }
                }
            }
        });
    }

    if (type === "ustensils") {
        content.forEach((list) => {
            list.ustensils.forEach((elem) => {
                if (ValueReturned.includes(elem) === false) {
                    if (valueFiltered == "nothing") {
                        ValueReturned.push(elem);
                    } else {
                        let applianceOk = elem
                            .toLowerCase()
                            .includes(valueFiltered);

                        if (applianceOk === true) {
                            ValueReturned.push(elem);
                        }
                    }
                }
            });
        });
    }

    return ValueReturned;
}

/**
 * Add filter
 * @param {Array} filter
 * @param {string} type
 */
function addFilter(filter, type) {
    //---- STYLE ----//
    if (!filter.classList.contains("active")) {
        filter.classList.add("active");
        HTML_filterPill(type, filter.getAttribute("data-value"));
    }

    //---- PROCESS ----//
    if (type == "ingredients") {
        ingredients.push(filter.getAttribute("data-value").toLowerCase());
    }

    if (type == "appliances") {
        appliances.push(filter.getAttribute("data-value").toLowerCase());
    }

    if (type == "ustensils") {
        ustensils.push(filter.getAttribute("data-value").toLowerCase());
    }
}

/**
 * delete filter
 * @param {Array} filter
 * @param {string} type
 */
function deleteFilter(filter, type) {
    filter.remove();

    let options = document.querySelectorAll(".options_container .option");
    options.forEach((option) => {
        if (option.parentElement.classList.contains(type)) {
            if (type == "ingredients") {
                const index = ingredients.indexOf(
                    filter.getAttribute("data-value").toLowerCase()
                );

                if (index > -1) {
                    ingredients.splice(index, 1);
                }
            }

            if (type == "appliances") {
                const index = appliances.indexOf(
                    filter.getAttribute("data-value").toLowerCase()
                );

                if (index > -1) {
                    appliances.splice(index, 1);
                }
            }

            if (type == "ustensils") {
                const index = ustensils.indexOf(
                    filter.getAttribute("data-value").toLowerCase()
                );

                if (index > -1) {
                    ustensils.splice(index, 1);
                }
            }
        }

        if (
            filter.getAttribute("data-value") ==
            option.getAttribute("data-value")
        ) {
            if (option.classList.contains("active")) {
                option.classList.remove("active");
            }
        }
    });
}

/**
 * match ingredients select with mainbar
 * @param {Array} recipes
 * @param {Array} ingredients
 * @returns {Array} recipesMatched
 */
function matchIngredients(recipes, ingredients) {
    let recipesMatched = [];
    for (let recipe of recipes) {
        let ingredientsMatch = [];
        ingredients.forEach((ingredient) => {
            ingredientsMatch.push(
                recipe.ingredients.filter((recIngredient) =>
                    recIngredient.ingredient
                        .toLowerCase()
                        .includes(ingredient.toLowerCase())
                ).length > 0
            );
        });
        if (ingredientsMatch.every((match) => match == true)) {
            recipesMatched.push(recipe);
        }
    }

    return recipesMatched;
}

/**
 * match ustensils select with mainbar
 * @param {Array} recipes
 * @param {Array} ustensils
 * @returns {Array} recipesMatched
 */
function matchUstensils(recipes, ustensils) {
    let recipesMatched = [];
    for (let recipe of recipes) {
        let UstensilMatch = [];

        ustensils.forEach((ustensil) => {
            UstensilMatch.push(
                recipe.ustensils.filter((recUstensil) =>
                    recUstensil.toLowerCase().includes(ustensil.toLowerCase())
                ).length > 0
            );
        });
        if (UstensilMatch.every((match) => match == true)) {
            recipesMatched.push(recipe);
        }
    }
    return recipesMatched;
}

/**
 * match appliances select with mainbar
 * @param {Array} recipes
 * @param {Array} appliances
 * @returns {Array} recipesMatched
 */
function matchAppliance(recipes, appliance_select) {
    let recipesMatched = [];

    for (let recipe of recipes) {
        if (recipe.appliance.toLowerCase().includes(appliance_select)) {
            recipesMatched.push(recipe);
        }
    }

    return recipesMatched;
}

/**
 * open filterbox
 * @param {Bool} element
 */
function clickOption(element) {
    let val = element.getAttribute("data-value");
    let drop = element.parentElement;
    const prev =
        element.parentElement.childNodes[3].classList.contains("active");

    let prevActive = null;
    if (prev === true) {
        prevActive =
            element.parentElement.childNodes[3].getAttribute("data-value");
    }
    let options = document.querySelector(".drop .option").length;

    drop.querySelectorAll(".option.active")[0].classList.add("mini-hack");
    drop.classList.toggle("visible");
    drop.classList.toggle("withBG");
    element.style.top;
    drop.classList.toggle("opacity");

    document.querySelector(".mini-hack").classList.remove("mini-hack");

    if (drop.classList.contains("visible")) {
        setTimeout(function () {
            drop.classList.add("withBG");
        }, 400 + options * 100);
    }

    this.triggerAnimation(drop);
    if (val !== "placeholder" || prevActive === "placeholder") {
        document.querySelector(".option_click").classList.remove("active");
        element.classList.add("active");
    }
}

/**
 * trigger animation from filterbox
 * @param {String} drop
 */
function triggerAnimation(drop) {
    let finalWidth = drop.classList.contains("visible") ? 25 : 10;
    drop.style.width = "10em";
    setTimeout(function () {
        drop.style.width = finalWidth + "em";
    }, 400);
}

//------------------- Open filter searchbox -----------------------//
document
    .querySelector(".click_primary")
    .addEventListener("click", function (event) {
        const targetElement = event.target || event.srcElement;
        clickOption(targetElement);
    });

document
    .querySelector(".click_secondary")
    .addEventListener("click", function (event) {
        const targetElement = event.target || event.srcElement;
        clickOption(targetElement);
    });

document
    .querySelector(".click_tiertary")
    .addEventListener("click", function (event) {
        const targetElement = event.target || event.srcElement;
        clickOption(targetElement);
    });
//----------------------------------------------------------------

//-------- On focus or not focus filter searchbox -----------//
const serachBox_primary = document.getElementById("search_primary");
const serachBox_secondary = document.getElementById("search_secondary");
const serachBox_tiertary = document.getElementById("search_tiertary");

serachBox_primary.addEventListener("focusin", (element) => {
    if (serachBox_primary.value === "Ingredients") {
        element["target"].value = "";
    }
});

serachBox_secondary.addEventListener("focusin", (element) => {
    if (serachBox_primary.value === "Appareil") {
        element["target"].value = "";
    }
});

serachBox_tiertary.addEventListener("focusin", (element) => {
    if (serachBox_primary.value === "Ustensiles") {
        element["target"].value = "";
    }
});
//----------------------------------------------------------------