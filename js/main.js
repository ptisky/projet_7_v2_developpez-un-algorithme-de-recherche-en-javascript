//------------------- Global variables used -----------------------//
let mainBar = [];
let ingredients = [];
let appliances = [];
let ustensils = [];
let allRecipes = [];
//----------------------------------------------------------------


//-------- Load MAIN SCRIPT -----------//
loadJSON(function (response) {
    const getAllrecipes = JSON.parse(response);
    const recipes = getAllrecipes.recipes;
    allRecipes = recipes;

    //---Init MAIN SCRIPT ( fill filters & recipes )
    fillRecipes(recipes);
    HTML_filterBox("ingredients", getFilter(recipes, "ingredients"));
    HTML_filterBox("appliances", getFilter(recipes, "appliances"));
    HTML_filterBox("ustensils", getFilter(recipes, "ustensils"));

    //---When enter key on main searchbar
    const serachBox = document.getElementById("searchbar");
    serachBox.addEventListener("keyup", (key) => {
        search(recipes);
    });

    //---When search specific filter
    serachBox_primary.addEventListener("keyup", (key) => {
        const value = key.target.value.toLowerCase();

        if (value.length >= 3) {
            HTML_filterBox(
                "ingredients",
                getFilter(recipes, "ingredients", value)
            );
        } else {
            HTML_filterBox("ingredients", getFilter(recipes, "ingredients"));
        }
    });

    serachBox_secondary.addEventListener("keyup", (key) => {
        const value = key.target.value.toLowerCase();

        if (value.length >= 3) {
            HTML_filterBox(
                "appliances",
                getFilter(recipes, "appliances", value)
            );
        } else {
            HTML_filterBox("appliances", getFilter(recipes, "appliances"));
        }
    });

    serachBox_tiertary.addEventListener("keyup", (key) => {
        const value = key.target.value.toLowerCase();

        if (value.length >= 3) {
            HTML_filterBox("ustensils", getFilter(recipes, "ustensils", value));
        } else {
            HTML_filterBox("ustensils", getFilter(recipes, "ustensils"));
        }
    });
});
//----------------------------------------------------------------
