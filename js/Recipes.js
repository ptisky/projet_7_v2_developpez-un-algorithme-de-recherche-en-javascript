/**
 * Check key enter from main searchbar
 * @param {String} key
 * @param {Array} recipes
 */
function matchFromMainbar(key, recipes) {
    const value = key.toLowerCase();

    if (value.length >= 3) {
        let filtered = recipes.filter((recipe) => {
            return (
                recipe.name.toLowerCase().includes(value) || // search by recette name
                recipe.ingredients.some((i) =>
                    i.ingredient.toLowerCase().includes(value)
                ) || // search by ingredients
                recipe.ustensils.some((u) => u.toLowerCase().includes(value)) || // search by ustensils
                recipe.appliance.toLowerCase().includes(value) || // search by appliances
                recipe.description.toLowerCase().includes(value) // search by description
            );
        });

        mainBar = filtered;
    } else {
        mainBar = recipes;
    }
}

/**
 * Display Recipes
 * @param {Array} allRecipes
 */
function fillRecipes(allRecipes) {
    allRecipes.forEach((recipe) => {
        let list_ingredient = "";
        recipe.ingredients.forEach((element) => {
            const ingredient = element.ingredient;
            let quantity = "";

            if (element.unit != undefined) {
                quantity = element.quantity + " " + element.unit;
            } else if (element.quantity != undefined) {
                quantity = element.quantity;
            } else {
                quantity = "";
            }

            list_ingredient += `<p><b>${ingredient}:</b> <span>${quantity}</span></p>`;
        });

        document.querySelector("#recipe").innerHTML += `
                <section class="card_recipe">
                    <img src="https://via.placeholder.com/600x400" alt="" />
                    <div class="recipe_desc_container">
                        <h2>${recipe.name}</h2>
                        <em><img src="img/watch.svg"> ${recipe.time}min</em>
                        <div class="list_recipe">${list_ingredient}</div>
                        <div class="line-clamp">
                            <p class="desc_recipe">${recipe.description}</p>
                        </div>
                    </div>
                </section>
            `;
    });
}

/**
 * Main search algo
 * @param {Array} allRecipes
 */
function search(allRecipes) {
    document.querySelector("#recipe").innerHTML = "";

    let key = document.querySelector("#searchbar").value;
    if (key.length >= 3) {
        mainbar = matchFromMainbar(key, allRecipes);
        mainBar = matchAppliance(mainBar, appliances);
        mainBar = matchUstensils(mainBar, ustensils);
        mainBar = matchIngredients(mainBar, ingredients);

        if (mainBar == "") {
            noRecipes();
        } else {
            fillRecipes(mainBar);
        }
    } else {
        allRecipes = matchAppliance(allRecipes, appliances);
        allRecipes = matchUstensils(allRecipes, ustensils);
        allRecipes = matchIngredients(allRecipes, ingredients);

        if (allRecipes == "") {
            noRecipes();
        } else {
            fillRecipes(allRecipes);
        }
    }
}
